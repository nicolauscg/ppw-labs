from django.shortcuts import render
from datetime import datetime, date

mhs_name = 'Nicolaus Christian Gozali'
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(2000, 5, 11)
npm = '1706020446'
university = 'Universitas of Indonesia'
hobby = 'play games'
description = 'just a cs student'

# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'university': university, 'hobby': hobby, 'description': description}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
